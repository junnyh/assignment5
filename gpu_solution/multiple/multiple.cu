#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/functional.h>
#include <iostream>
#include "../cputimer.h"
#include "../csv.h"

int main() {
    // timer for elapsed time
    Timer timer_1, timer_2;
    timer_1.Start();

    // parse .csv inputs to three arrays
    io::CSVReader<3> infile("final.csv");
    float y0, x1, x2;
    const float _n = (float) 1 / 173179759;

    // create and fill Y, X1, X2 vector on host
    thrust::host_vector<float> h_y0, h_x1, h_x2;
    while (infile.read_row(y0, x1, x2)) {
        h_y0.push_back(y0);
        h_x1.push_back(x1);
        h_x2.push_back(x2);
    }

    timer_1.Stop();
    float elapsed_1 = timer_1.ElapsedMillis(); // parsing time
    timer_2.Start();

    // create device arrays and copy arrays from host to device
    thrust::device_vector<float> d_y0 = h_y0, d_x1 = h_x1, d_x2 = h_x2;

    // term x2_square
    thrust::device_vector<float> d_temp(d_x2.size());
    thrust::transform(
        d_x2.begin(), d_x2.end(), d_x2.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float x2_square = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // term x1_square
    thrust::transform(
        d_x1.begin(), d_x1.end(), d_x1.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float x1_square = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // term sum_x2, sum_x1, sum_y
    float sum_x2 = thrust::reduce(
        d_x2.begin(), d_x2.end(), (float) 0.0, thrust::plus<float>());
    float sum_y = thrust::reduce(
        d_y0.begin(), d_y0.end(), (float) 0.0, thrust::plus<float>());
    float sum_x1 = thrust::reduce(
        d_x1.begin(), d_x1.end(), (float) 0.0, thrust::plus<float>());

    // term sum_x1y
    thrust::transform(
        d_y0.begin(), d_y0.end(), d_x1.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float sum_x1y = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // term sum_x1x2
    thrust::transform(
        d_x2.begin(), d_x2.end(), d_x1.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float sum_x1x2 = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // term sum_x2y
    thrust::transform(
        d_y0.begin(), d_y0.end(), d_x2.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float sum_x2y = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // centered terms
    x1_square = x1_square - sum_x1 * sum_x1 * _n;
    x2_square = x2_square - sum_x2 * sum_x2 * _n;
    sum_x1y = sum_x1y - sum_x1 * sum_y * _n;
    sum_x2y = sum_x2y - sum_x2 * sum_y * _n;
    sum_x1x2 = sum_x1x2 - sum_x1 * sum_x2 * _n;

    float b1 = (x2_square * sum_x1y - sum_x1x2 * sum_x2y) /
        (x1_square * x2_square - sum_x1x2 * sum_x1x2);
    float b2 = (x1_square * sum_x2y - sum_x1x2 * sum_x1y) /
        (x1_square * x2_square - sum_x1x2 * sum_x1x2);
    float b0 = sum_y * _n - sum_x1 * _n * b1 - sum_x2 * _n * b2;

    timer_2.Stop();
    float elapsed_2 = timer_2.ElapsedMillis(); // processing time

    // display solution
    std::cout << "b0 = " << b0 << std::endl;
    std::cout << "b1 = " << b1 << std::endl;
    std::cout << "b2 = " << b2 << std::endl;

    // display time
    std::cout << "-- read input time: " << elapsed_1 << std::endl;
    std::cout << "-- processing time: " << elapsed_2 << std::endl;

    return 0;
}
