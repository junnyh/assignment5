#include <thrust/host_vector.h> 
#include <thrust/device_vector.h> 
#include <thrust/copy.h>
#include <thrust/fill.h> 
#include <thrust/sequence.h> 
#include <thrust/sort.h>
#include <iostream> 
#include <fstream>
#include <stdlib.h>
#include <string>
#include <ctime>

int main() {
  std::string line;
  std::ifstream file("input.txt");
  thrust::host_vector<double> h_array;
  std::clock_t start_1, start_2;
  double elapsed_1, elapsed_2 = 0.0;

  if (file.is_open())
  {
    start_1 = std::clock();
    // pase input file into vector
    while (std::getline(file, line))
    {
	double num = atof(line.c_str());
	h_array.push_back(num);
    }
    file.close();
    
    start_2 = std::clock();
    // sort device vector and copy back
    thrust::device_vector<double> d_array = h_array;
    thrust::sort(d_array.begin(), d_array.end());
    thrust::copy(d_array.begin(), d_array.end(), h_array.begin());

    elapsed_1 = (std::clock() - start_1) / (double) CLOCKS_PER_SEC;
    elapsed_2 = (std::clock() - start_2) / (double) CLOCKS_PER_SEC;
    std::cout << "-- parse time: " << elapsed_1 << std::endl;
    std::cout << "-- sort time:  " << elapsed_2 << std::endl;
  }
  else
  {
     std::cerr << "Unable to open file." << std::endl;
  }

  // print out quantiles
  for (int i = 0; i < 10; i++) {
    int idx = (int) (0.1 * i * h_array.size()); 
    std::cout << "A[" << idx << "] = " << h_array[idx] << std::endl; 
  }
  std::cout << "A[" << h_array.size() - 1 << "] = " 
            << h_array[h_array.size()-1] << std::endl;

  return 0; 
}

