#include <time.h>

struct Timer {
#if defined(_WIN32) || defined(_WIN64)

    LARGE_INTEGER ll_freq;
    LARGE_INTEGER ll_start;
    LARGE_INTEGER ll_stop;

    Timer() {
        QueryPerformanceFrequency(&ll_freq);
    }

    void Start() {
        QueryPerformanceCounter(&ll_start);
    }

    void Stop() {
        QueryPerformanceCounter(&ll_stop);
    }

    float ElapsedMillis() {
        double start = double(ll_start.QuadPart) / double(ll_freq.QuadPart);
        double stop  = double(ll_stop.QuadPart) / double(ll_freq.QuadPart);

        return (stop - start) * 1000;
    }

#elif defined(CLOCK_PROCESS_CPUTIME_ID)

    timespec start;
    timespec stop;

    void Start() {
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
    }

    void Stop() {
        clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
    }

    float ElapsedMillis() {
        timespec temp;
        if ((stop.tv_nsec-start.tv_nsec)<0) {
            temp.tv_sec = stop.tv_sec-start.tv_sec-1;
            temp.tv_nsec = 1000000000+stop.tv_nsec-start.tv_nsec;
        } else {
            temp.tv_sec = stop.tv_sec-start.tv_sec;
            temp.tv_nsec = stop.tv_nsec-start.tv_nsec;
        }
        return temp.tv_nsec/1000000.0;
    }

#else

    rusage start;
    rusage stop;

    void Start() {
        getrusage(RUSAGE_SELF, &start);
    }

    void Stop() {
        getrusage(RUSAGE_SELF, &stop);
    }

    float ElapsedMillis() {
        float sec = stop.ru_utime.tv_sec - start.ru_utime.tv_sec;
        float usec = stop.ru_utime.tv_usec - start.ru_utime.tv_usec;

        return (sec * 1000) + (usec / 1000);
    }

#endif
};
