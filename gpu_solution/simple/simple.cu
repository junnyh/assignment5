#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/functional.h>
#include <iostream>
#include "../cputimer.h"
#include "../csv.h"

int main() {
    // timer for elapsed time
    Timer timer_1, timer_2;
    timer_1.Start();

    // parse .csv inputs to three arrays
    io::CSVReader<3> infile("final.csv");
    float y0, x1, x2;
    const float _n = (float) 1 / 173179759;

    // create and fill Y, X1, X2 vector on host
    thrust::host_vector<float> h_y0, h_x1, h_x2;
    while (infile.read_row(y0, x1, x2)) {
        h_y0.push_back(y0);
        h_x1.push_back(x1);
        h_x2.push_back(x2);
    }

    timer_1.Stop();
    float elapsed_1 = timer_1.ElapsedMillis(); // parsing time
    timer_2.Start();

    // create device arrays and copy arrays from host to device
    thrust::device_vector<float> d_y0 = h_y0, d_x1 = h_x1, d_x2 = h_x2;

    //
    // calculate b1
    //

    // term 0
    thrust::device_vector<float> d_temp(d_y0.size());
    thrust::copy(d_x2.begin(), d_x2.end(), d_temp.begin());
    thrust::transform(
        d_y0.begin(), d_y0.end(), d_temp.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float term_0 = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // std::cout << term_0 << std::endl;

    // term 1
    float sum_x2 = thrust::reduce(
        d_x2.begin(), d_x2.end(), (float) 0.0, thrust::plus<float>());
    float sum_y0 = thrust::reduce(
        d_y0.begin(), d_y0.end(), (float) 0.0, thrust::plus<float>());
    float term_1 = _n * sum_x2 * sum_y0;

    // std::cout << sum_x2 << " " << sum_y0 << " " << term_1 << std::endl;

    // term 2
    thrust::copy(d_x2.begin(), d_x2.end(), d_temp.begin());
    thrust::transform(
        d_temp.begin(), d_temp.end(), d_temp.begin(), d_temp.begin(),
        thrust::multiplies<float>());
    float term_2 = thrust::reduce(
        d_temp.begin(), d_temp.end(), (float) 0.0, thrust::plus<float>());

    // std::cout << term_2 << std::endl;

    // term 3
    float term_3 = thrust::reduce(
        d_x2.begin(), d_x2.end(), (float) 0.0, thrust::plus<float>());
    term_3 = term_3 * term_3 * _n;

    // std::cout << term_3 << std::endl;

    float b1 = (term_0 - term_1) / (term_2 - term_3);

    //
    // calculate b0
    //
    float b0 = _n * sum_y0 - _n * b1 * sum_x2;

    timer_2.Stop();
    float elapsed_2 = timer_2.ElapsedMillis(); // processing time

    // display solution
    std::cout << "b0 = " << b0 << std::endl;
    std::cout << "b1 = " << b1 << std::endl;

    // display time
    std::cout << "-- read input time: " << elapsed_1 << std::endl;
    std::cout << "-- processing time: " << elapsed_2 << std::endl;

    return 0;
}
