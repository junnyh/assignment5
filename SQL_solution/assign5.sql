CREATE TABLE taxi_reg
(amount_diff NUMERIC(12,3),
surchage NUMERIC(12,3),
trip_time NUMERIC(12,3));

\copy taxi_reg FROM '/data/zejunh/Assignment5/data/final.csv' DELIMITER ',';

SELECT ntile, 
min(amount_diff) AS min_diff,
max(amount_diff) AS max_diff
FROM (SELECT amount_diff, ntile(10) OVER (ORDER BY amount_diff) AS ntile FROM taxi_reg ) x
GROUP BY ntile
ORDER BY ntile;

%2. Regression

SELECT REGR_INTERCEPT(amount_diff, trip_time),
REGR_SLOPE(amount_diff, trip_time)
FROM taxi_reg;

% 2 variable regression
ALTER TABLE taxi_reg
ADD COLUMN X1Y NUMERIC(12,3);

ALTER TABLE taxi_reg
ADD COLUMN X2Y NUMERIC(12,3);

ALTER TABLE taxi_reg
ADD COLUMN X2X2 NUMERIC(12,3);

UPDATE taxi_reg
SET X1Y = amount_diff * surcharge, 
X2Y = trip_time * amount_diff,
X1X2 = trip_time * surcharge;

SELECT sum(surcharge*surcharge) AS x1_square, sum(trip_time*trip_time) AS x2_square, sum(x1y) AS sum_x1y, sum(x2y) as sum_x2y, sum(x1x2) as sum_x1x2, sum(surcharge) AS sum_x1, sum(trip_time) AS sum_x2, sum(amount_diff) AS sum_y,
INTO reg_tmp
FROM taxi_reg;

ALTER TABLE reg_tmp
ADD COLUMN n INT;

UPDATE reg_tmp
SET n = 173179759;

UPDATE reg_tmp
SET x1_square = x1_square - sum_x1 * sum_x1 / n,
x2_square = x2_square - sum_x2 * sum_x2 / n;

UPDATE reg_tmp
SET sum_x1y = sum_x1y - sum_x1*sum_y / n,
sum_x2y = sum_x2y - sum_x2*sum_y / n,
sum_x1x2 = sum_x1x2 - sum_x1 * sum_x2 / n;

UPDATE reg_tmp
SET x1_square = x1_square - sum_x1 * sum_x1 / n,
x2_square = x2_square - sum_x2 * sum_x2 / n;

SELECT (x2_square * sum_x1y - sum_x1x2 * sum_x2y)/(x1_square*x2_square - sum_x1x2*sum_x1x2) AS b1
FROM reg_tmp;

SELECT (x1_square * sum_x2y - sum_x1x2*sum_x1y)/(x1_square*x2_square - sum_x1x2*sum_x1x2) AS b2
FROM reg_tmp;

SELECT sum_y/n - sum_x1 / n * 0.30410458750363967076 - sum_x2 / n * 0.00202196224895446451 AS b0
FROM reg_tmp;

