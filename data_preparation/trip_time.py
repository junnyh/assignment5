import time
from datetime import datetime

with open("/data/zejunh/Assignment5/data/trip_data_all.csv") as fIn, open("/data/zejunh/Assignment5/data/trip_time.txt",'a') as fOut: 
	next(fIn)
	for line in fIn:
		if len(line) > 1:
			xlist = line.split(',')
			#print xlist
			time1 = datetime.strptime(xlist[5], '%Y-%m-%d %H:%M:%S')
			time2 = datetime.strptime(xlist[6], '%Y-%m-%d %H:%M:%S')
			trip_time = (time2 - time1).total_seconds()
			new_line = str(trip_time) + '\n'
			fOut.write(new_line)
fIn.close()
fOut.close()
