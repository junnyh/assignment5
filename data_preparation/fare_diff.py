import time
from datetime import datetime

with open("/data/zejunh/Assignment5/data/trip_fare_all.csv") as fIn, open("/data/zejunh/Assignment5/data/trip_fare.csv",'a') as fOut: 
	next(fIn)
	for line in fIn:
		ylist = line.split(',')
		if len(line) > 1:
	 		amount1 = ylist[9]
                        amount2 = ylist[10]
                        amount_diff = float(amount2) - float(amount1)
                        surcharge = float(ylist[6])
			new_line = str(amount_diff) + ',' + str(surcharge) + '\n'
			fOut.write(new_line)
fIn.close()
fOut.close()
